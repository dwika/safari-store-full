firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    initCategories();
    initProducts();
    initHelp();

    initActionTab();
  } else {
    window.location.href="login";
  }
});

function initActionTab() {
  $('#tab-categories').click(function(){
    initCategories();
  });
  $('#tab-products').click(function(){
    initProducts();
  });
  $('#tab-help').click(function(){
    initHelp();
  });
}
