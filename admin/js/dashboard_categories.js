var parentCategories = $('#widget-categories');

var childEmptyCategories =
    '<li>\n' +
    '<input class="form-control form-control-rounded form-control-sm widget-categories-child-input d-inline-block" type="text" placeholder="Categories">\n' +
    '&nbsp; &nbsp;\n' +
    '<button onclick="addingCategories(this);" class="btn btn-sm btn-primary" type="button"><i class="icon-check"></i></button>\n' +
    '</li>\n';
function childCategories(categoriesName, categoriesCount, children) {
    return '' +
    // '<li class="has-children"><a onclick="actionAccordion(this);">'+ categoriesName +'</a><span>('+ categoriesCount +')</span>&nbsp;<i onclick="deleteCategoriesParent(this);" class="icon-circle-minus"></i>\n' +
    '<li class="has-children"><a onclick="actionAccordion(this);">'+ categoriesName +'</a>&nbsp;<i onclick="deleteCategoriesParent(this);" class="icon-circle-minus"></i>\n' +
    '<ul>\n' +
    children + '\n' +
    '</ul>\n' +
    '</li>\n';
}

var childEmptySubCategories =
    '<li>\n' +
    '<input class="form-control form-control-rounded form-control-sm widget-categories-child-input d-inline-block" type="text" placeholder="Sub Categories">\n' +
    '&nbsp; &nbsp;\n' +
    '<button onclick="addingSubCategories(this);" class="btn btn-sm btn-primary" type="button"><i class="icon-check"></i></button>\n' +
    '</li>\n';
function childSubCategories(subCategoriesName, subCategoriesCount, children) {
    return '' +
    // '<li><a>'+ subCategoriesName +'</a><span>('+ subCategoriesCount +')</span>\n' +
    '<li><a>'+ subCategoriesName +'</a>\n' +
    '<ul>\n' +
    children + '\n' +
    '</ul>\n' +
    '</li>\n';
}

function childCategoriesList(childName, childCount) {
  // return '<li><a>'+ childName +'</a><span>('+ childCount +')</span>&nbsp;<i onclick="deleteCategoriesChild(this);" class="icon-circle-minus"></i>';
  return '<li><a>'+ childName +'</a>&nbsp;<i onclick="deleteCategoriesChild(this);" class="icon-circle-minus"></i>';
}

// INIT STATE
function initCategories() {
  resetCategories();
  callCategories();
}

// INIT CALLING
function callCategories() {
  categoriesRef.get().then(function(listOfCategories) {
    if (listOfCategories.size <= 0) {
      parentCategories.append(childEmptyCategories);
    } else {
      readCategories(listOfCategories);
    }
  });
}

//
//  ADDING
//

function addingCategories(view) {
  var categoriesView = $(view).parent();
  var categoriesInputView = categoriesView.children("input");
  var categoriesValue = categoriesInputView.val().toLowerCase();

  var buttonView = view;
  $(buttonView).html(spinnerValue);

  if (categoriesValue.length <=0) {
    alert("Categories cannot be empty!");
    $(buttonView).html(checkValue);
    return;
  }

  categoriesRef.where("name", "==", categoriesValue)
  .get().then(function(listOfCategories) {
    if (listOfCategories.size <= 0) {
      categoriesRef.add({
        name: categoriesValue
      })
      .then(function() {
        $(childCategories(categoriesValue, 0, childEmptySubCategories)).insertBefore(categoriesView);
        clearInput(categoriesInputView);
        $(buttonView).html(checkValue);
        toast(toastIconSuccess, toastTypeSuccess, "Success", "Berhasil menambahkan categories!");
      });
    } else {
      listOfCategories.forEach(function(doc) {
          alert(doc.data().name + " is exist in categories, try another name");
          $(buttonView).html(checkValue);
      });
    }
  });
}

function addingSubCategories(view) {
  var subCategoriesView = $(view).parent();
  var subCategoriesInputView = subCategoriesView.children("input");
  var categoriesValue = subCategoriesView.parent().parent().children("a").text().toLowerCase();
  var subCategoriesValue = subCategoriesInputView.val().toLowerCase();

  var prefixCode = categoriesValue.charAt(0).concat(subCategoriesValue.charAt(0));
  var buttonView = view;
  $(buttonView).html(spinnerValue);

  if (subCategoriesValue.length <=0) {
    alert("Sub categories cannot be empty!");
    $(buttonView).html(checkValue);
    return;
  }

  categoriesRef.where("name", "==", categoriesValue)
  .get().then(function(listOfCategories) {
    listOfCategories.forEach(function(doc) {
      categoriesRef.doc(doc.id)
      .collection("subCategories").where("name", "==", subCategoriesValue)
      .get().then(function(listOfSubCategories) {
        if (listOfSubCategories.size <= 0) {
          categoriesRef.doc(doc.id)
          .collection("subCategories").add({
            name: subCategoriesValue,
            code: prefixCode
          })
          .then(function() {
            $(childCategoriesList(subCategoriesValue, 0)).insertBefore(subCategoriesView);
            clearInput(subCategoriesInputView);
            $(buttonView).html(checkValue);
            toast(toastIconSuccess, toastTypeSuccess, "Success", "Berhasil menambahkan subCategories!");
          });
        } else {
          listOfSubCategories.forEach(function(doc) {
              alert(doc.data().name + " is exist in subCategory "+ categoriesValue +", try another name");
              $(buttonView).html(checkValue);
          });
        }
      });
    });
  });
}

//
//  READ
//

function readCategories(listOfCategories) {
  listOfCategories.forEach(function(doc) {
    categoriesRef.doc(doc.id)
    .collection("subCategories")
    .get().then(function(listOfSubCategories) {
      if (listOfSubCategories.size <= 0) {
        parentCategories.prepend(childCategories(doc.data().name, 0, childEmptySubCategories));
      } else {
        readSubCategories(doc.data().name, listOfSubCategories);
      }
    });
  });
  parentCategories.append(childEmptyCategories);
}

function readSubCategories(categories, listOfSubCategories) {
  var children = "";
  listOfSubCategories.forEach(function(doc) {
    children = children.concat(childCategoriesList(doc.data().name, 0));
  });
  children = children.concat(childEmptySubCategories);
  parentCategories.prepend(childCategories(categories, listOfSubCategories.size, children));
}

//
//  DELETE
//

function deleteCategoriesParent(view) {
  var categoriesView = $(view).parent();
  var categoriesValue = categoriesView.children("a").text().toLowerCase();

  categoriesRef.where("name", "==", categoriesValue)
  .get().then(function(listOfCategories) {
      listOfCategories.forEach(function(doc) {
        categoriesRef.doc(doc.id)
        .collection("subCategories")
        .get().then(function(listOfSubCategories) {
            listOfSubCategories.forEach(function(subdoc) {
              categoriesRef.doc(doc.id)
              .collection("subCategories").doc(subdoc.id)
              .delete();
            })
        });

        categoriesRef.doc(doc.id)
        .delete().then(function() {
          categoriesView.empty();
          toast(toastIconSuccess, toastTypeSuccess, "Success", "Berhasil menghapus categories!");
        });
      });
  });
}

function deleteCategoriesChild(view) {
  var subCategoriesView = $(view).parent();
  var categoriesValue = subCategoriesView.parent().parent().children("a").text().toLowerCase();
  var subCategoriesValue = subCategoriesView.children("a").text().toLowerCase();

  categoriesRef.get().then(function(listOfCategories) {
      listOfCategories.forEach(function(doc) {
        categoriesRef.doc(doc.id)
        .collection("subCategories").where("name", "==", subCategoriesValue)
        .get().then(function(listOfSubCategories) {
            listOfSubCategories.forEach(function(subdoc) {
              categoriesRef.doc(doc.id)
              .collection("subCategories").doc(subdoc.id)
              .delete().then(function() {
                subCategoriesView.empty();
                toast(toastIconSuccess, toastTypeSuccess, "Success", "Berhasil menghapus subCategories!");
              });
            })
        });
      });
  });
}

//
//  RESET
//

function resetCategories() {
  parentCategories.empty();
}
