var parentHelp = $('#widget-help');

var childEmptyHelpCategories = '' +
  '<li class="has-children">\n' +
    '<input class="form-control form-control-rounded form-control-sm widget-categories-child-input d-inline-block" type="text" placeholder="Help Categories">\n' +
    '&nbsp; &nbsp;\n' +
    '<button onclick="addingHelpCategories(this);" class="btn btn-sm btn-primary" type="button"><i class="icon-check"></i></button>\n' +
  '</li>\n';

function childEmptyQuestionAnswer(id, helpCategories, answerQuestions) {
  return '' +
  '<li id="'+ id +'" class="has-children expanded">\n' +
    '<a onclick="actionAccordion(this);">'+ helpCategories +'</a>\n' +
    '&nbsp;\n' +
    '<i onclick="deleteHelpCategoriesView(this);" class="icon-circle-minus"></i>\n' +
    '<ul>\n' +
      answerQuestions +
      '<li>\n' +
        '<input class="form-control form-control-rounded form-control-sm widget-categories-child-input" type="text"  placeholder="Question">\n' +
        '<textarea class="form-control form-control-rounded form-control-sm widget-categories-child-textarea" rows="3" placeholder="Answer"></textarea>\n' +
        '<br>\n' +
        '<button onclick="addingQuestionAnswer(this)" class="btn btn-sm btn-primary" type="button">\n' +
            'Add Question Answer\n' +
            '&nbsp;\n' +
            '<i class="icon-plus"></i>\n' +
        '</button>\n' +
        '<br><br>\n' +
      '</li>\n' +
    '</ul>\n' +
  '</li>\n';
}

function childQuestionAnswer(id, question, answer) {
  return '' +
  '<li id="'+ id +'" class="variant-added">\n' +
    '<a>'+ question +'</a>\n' +
    '&nbsp;\n' +
    '<i onclick="deleteQuestionAnswerView(this);" class="icon-circle-minus"></i>\n' +
    '<textarea class="form-control form-control-rounded form-control-sm widget-categories-child-textarea" rows="3" disabled>'+ answer +'</textarea>\n' +
    '<br>\n' +
  '</li>\n';
}

function initHelp() {
  resetHelp();
  callHelpCategories();
}

function callHelpCategories() {
  // getAllHelpCategories(
  getAllHelpCategories(
    function(mapOfHelpCategories) {
      readHelpCategories(mapOfHelpCategories);
    },
    function() {
      parentHelp.append(childEmptyHelpCategories);
    }
  );
}

function addingHelpCategories(view) {
  var helpCategoriesView = $(view).parent();
  var helpCategoriesInputView = helpCategoriesView.children("input");
  var helpCategoriesValue = helpCategoriesInputView.val();

  if (isViewBlank(helpCategoriesInputView)) { toast(toastIconDanger, toastTypeDanger, "Alert", "Fill the Help Categories!"); return; }

  btnLoading(view, true);
  existFireData(helpRef, "name", helpCategoriesValue,
    function() {
      var createdAt = getTimeMillis();
      addFireData(helpRef,
        {
          name: helpCategoriesValue,
          createdAt: createdAt,
          updatedAt: createdAt
        },
        function() {
          $(childEmptyQuestionAnswer(createdAt, helpCategoriesValue, "")).insertBefore(helpCategoriesView);
          toast(toastIconSuccess, toastTypeSuccess, "Success", "Berhasil menambahkan help categories!");
          btnLoading(view, false);
          clearInput(helpCategoriesInputView);
      });
    },
    function(doc) {
      toast(toastIconDanger, toastTypeDanger, "Alert", doc.data().name + " is exist in help categories, try another help categories");
      btnLoading(view, false);
    }
  );
}

function addingQuestionAnswer(view) {
  var questionAnswerView = $(view).parent();
  var questionAnswerCategoriesView = questionAnswerView.parent().parent().children("a");
  var questionInputView = questionAnswerView.children("input");
  var answerTextAreaView = questionAnswerView.children("textarea");
  var categoriesValue = questionAnswerCategoriesView.text();
  var questionValue = questionInputView.val();
  var answerValue = answerTextAreaView.val();

  if (isViewBlank(questionInputView)) { toast(toastIconDanger, toastTypeDanger, "Alert", "Fill the Help Question!"); return; }
  if (isViewBlank(answerTextAreaView)) { toast(toastIconDanger, toastTypeDanger, "Alert", "Fill the Help Answer!"); return; }

  btnLoading(view, true);
  existFireData(helpRef, "name", categoriesValue,
    function() { },
    function(doc) {
      var createdAt = getTimeMillis();
      var answerQuestions = helpRef.doc(doc.id).collection("answerQuestions");
      addFireData(answerQuestions,
        {
          question: questionValue,
          answer: answerValue,
          createdAt: createdAt,
          updatedAt: createdAt
        },
        function() {
          $(childQuestionAnswer(createdAt, questionValue, answerValue)).insertBefore(questionAnswerView);
          toast(toastIconSuccess, toastTypeSuccess, "Success", "Berhasil menambahkan answer & questions!");
          btnLoading(view, false);
          clearInput(questionInputView);
          clearInput(answerTextAreaView);
        }
      );
    }
  );
}

function readHelpCategories(mapOfHelpCategories) {
  var arrOfKeysHelpCategories = [];
  var arrOfDataHelpCategories = [];
  readMap(mapOfHelpCategories, function(key, data) {
    arrOfKeysHelpCategories.push(key);
    arrOfDataHelpCategories.push(data);
  });

  var countComplete = 0;
  var countTotal = lengthArray(arrOfKeysHelpCategories);

  var getAnswerQuestions = function() {
    if (countComplete < countTotal) {
      var helpCategories = arrOfDataHelpCategories[countComplete].name;
      var updatedAt = arrOfDataHelpCategories[countComplete].updatedAt;
      var key = arrOfKeysHelpCategories[countComplete];
      getOnceQuestionAnswer(key, function(mapOfAnswerQuestions) {
        var listOfAnswerQuestionsView = "";
        readMap(mapOfAnswerQuestions, function(id, data) {
          listOfAnswerQuestionsView = listOfAnswerQuestionsView.concat(
            childQuestionAnswer(data.createdAt, data.question, data.answer)
          );
        });
        countComplete++;
        parentHelp.append(childEmptyQuestionAnswer(updatedAt, helpCategories, listOfAnswerQuestionsView));
        getAnswerQuestions();
      });
    } else {
      parentHelp.append(childEmptyHelpCategories);
    }
  };

  getAnswerQuestions();
}

function deleteHelpCategoriesView(view) {
  var parent = $(view).parent();
  var parentId = parent.attr('id');

  deleteHelpCategoriesWhere(parentId, function() {
    toast(toastIconSuccess, toastTypeSuccess, "Success", "Berhasil hapus Help Categories!");
    parent.remove();
  });
}

function deleteQuestionAnswerView(view) {
  var child = $(view).parent();
  var childId = child.attr('id');

  var parent = child.parent().parent();
  var parentId = parent.attr('id');

  deleteQuestionAnswerWhere(parentId, childId, function() {
    toast(toastIconSuccess, toastTypeSuccess, "Success", "Berhasil hapus Answer and Question!");
    child.remove();
  });
}

//
//  RESET
//

function resetHelp() {
  parentHelp.empty();
}
