var parentProductVariant = $('#widget-product-variant');

var childEmptyVariant =
    '<li>\n' +
    '<input class="form-control form-control-rounded form-control-sm widget-categories-child-input" type="text" placeholder="Variant">\n' +
    '&nbsp; &nbsp;\n' +
    '<button onclick="addingVariant(this);" class="btn btn-sm btn-primary" type="button"><i class="icon-check"></i></button>\n' +
    '</li>\n';
function childVariant(variantName, variantCount, children) {
    return '' +
    // '<li class="has-children"><a onclick="actionAccordion(this);">'+ variantName +'</a><span>('+ variantCount +')</span>&nbsp;<i onclick="deleteVarianParent(this);" class="icon-circle-minus"></i>\n' +
    '<li class="has-children"><a onclick="actionAccordion(this);">'+ variantName +'</a>&nbsp;<i onclick="deleteVarianParent(this);" class="icon-circle-minus"></i>\n' +
    '<ul>\n' +
    children + '\n' +
    '</ul>\n' +
    '</li>\n';
}

var childEmptyVariantItem =
    '<li>\n' +
    '<input class="form-control form-control-rounded form-control-sm widget-categories-child-input" type="text" placeholder="Variant Item">\n' +
    '&nbsp; &nbsp;\n' +
    '<button onclick="addingVariantItem(this);" class="btn btn-sm btn-primary" type="button"><i class="icon-check"></i></button>\n' +
    '</li>\n';
function childVariantItem(variantItemName, variantItemCount, children) {
    return '' +
    // '<li><a>'+ variantItemName +'</a><span>('+ variantItemCount +')</span>\n' +
    '<li><a>'+ variantItemName +'</a>\n' +
    '<ul>\n' +
    children + '\n' +
    '</ul>\n' +
    '</li>\n';
}

function childVariantList(childName, childCount) {
  // return '<li><a href="#">'+ childName +'</a><span>('+ childCount +')</span>&nbsp;<i onclick="deleteVariantItemChild(this);" class="icon-circle-minus"></i>';
  return '<li><a>'+ childName +'</a>&nbsp;<i onclick="deleteVariantItemChild(this);" class="icon-circle-minus"></i>';
}

// INIT STATE
function initVariant() {
  callVariant();
}

// INIT CALLING
function callVariant() {
  variantRef.get().then(function(listOfVariant) {
    if (listOfVariant.size <= 0) {
      parentProductVariant.append(childEmptyVariant);
    } else {
      readVariant(listOfVariant);
    }
  });
}

//
//  ADDING
//

function addingVariant(view) {
  var variantView = $(view).parent();
  var variantInputView = variantView.children("input");
  var variantValue = variantInputView.val().toLowerCase();

  var buttonView = view;
  $(buttonView).html(spinnerValue);

  if (variantValue.length <=0) {
    alert("Variant cannot be empty!");
    $(buttonView).html(checkValue);
    return;
  }

  variantRef.where("name", "==", variantValue)
  .get().then(function(listOfVariant) {
    if (listOfVariant.size <= 0) {
      variantRef.add({
        name: variantValue
      })
      .then(function() {
        $(childVariant(variantValue, 0, childEmptyVariantItem)).insertBefore(variantView);
        clearInput(variantInputView);
        $(buttonView).html(checkValue);
        toast(toastIconSuccess, toastTypeSuccess, "Success", "Berhasil menambahkan variant!");
      });
    } else {
      listOfVariant.forEach(function(doc) {
          alert(doc.data().name + " is exist in variant, try another name");
          $(buttonView).html(checkValue);
      });
    }
  });
}

function addingVariantItem(view) {
  var variantItemView = $(view).parent();
  var variantItemInputView = variantItemView.children("input");
  var variantValue = variantItemView.parent().parent().children("a").text().toLowerCase();
  var variantItemValue = variantItemInputView.val().toLowerCase();

  var buttonView = view;
  $(buttonView).html(spinnerValue);

  if (variantItemValue.length <=0) {
    alert("Variant item cannot be empty!");
    $(buttonView).html(checkValue);
    return;
  }

  variantRef.where("name", "==", variantValue)
  .get().then(function(listOfVariant) {
    listOfVariant.forEach(function(doc) {
      variantRef.doc(doc.id)
      .collection("items").where("name", "==", variantItemValue)
      .get().then(function(listOfVariantItem) {
        if (listOfVariantItem.size <= 0) {
          variantRef.doc(doc.id)
          .collection("items").add({
            name: variantItemValue
          })
          .then(function() {
            $(childVariantList(variantItemValue, 0)).insertBefore(variantItemView);
            clearInput(variantItemInputView);
            $(buttonView).html(checkValue);
            toast(toastIconSuccess, toastTypeSuccess, "Success", "Berhasil menambahkan variant item!");
          });
        } else {
          listOfVariantItem.forEach(function(doc) {
              alert(doc.data().name + " is exist in variant items "+ variantValue +", try another name");
              $(buttonView).html(checkValue);
          });
        }
      });
    });
  });
}

//
//  READ
//

function readVariant(listOfVariant) {
  parentProductVariant.empty();
  listOfVariant.forEach(function(doc) {
    variantRef.doc(doc.id)
    .collection("items")
    .get().then(function(listOfVariantItem) {
      if (listOfVariantItem.size <= 0) {
        parentProductVariant.prepend(childVariant(doc.data().name, 0, childEmptyVariantItem));
      } else {
        readVariantItem(doc.data().name, listOfVariantItem);
      }
    });
  });
  parentProductVariant.append(childEmptyVariant);
}

function readVariantItem(variant, listOfVariantItem) {
  var children = "";
  listOfVariantItem.forEach(function(doc) {
    children = children.concat(childVariantList(doc.data().name, 0));
  });
  children = children.concat(childEmptyVariantItem);
  parentProductVariant.prepend(childVariant(variant, listOfVariantItem.size, children));
}

//
//  DELETE
//

function deleteVarianParent(view) {
  var variantView = $(view).parent();
  var variantValue = variantView.children("a").text().toLowerCase();

  variantRef.where("name", "==", variantValue)
  .get().then(function(listOfVariant) {
      listOfVariant.forEach(function(doc) {
        variantRef.doc(doc.id)
        .collection("items")
        .get().then(function(listOfVariantItem) {
            listOfVariantItem.forEach(function(subdoc) {
              variantRef.doc(doc.id)
              .collection("items").doc(subdoc.id)
              .delete();
            })
        });

        variantRef.doc(doc.id)
        .delete().then(function() {
          variantView.empty();
          toast(toastIconSuccess, toastTypeSuccess, "Success", "Berhasil menghapus variant!");
        });
      });
  });
}

function deleteVariantItemChild(view) {
  var variantItemView = $(view).parent();
  var variantValue = variantItemView.parent().parent().children("a").text().toLowerCase();
  var variantItemValue = variantItemView.children("a").text().toLowerCase();

  variantRef.get().then(function(listOfVariant) {
      listOfVariant.forEach(function(doc) {
        variantRef.doc(doc.id)
        .collection("items").where("name", "==", variantItemValue)
        .get().then(function(listOfVariantItem) {
            listOfVariantItem.forEach(function(subdoc) {
              variantRef.doc(doc.id)
              .collection("items").doc(subdoc.id)
              .delete().then(function() {
                variantItemView.empty();
                toast(toastIconSuccess, toastTypeSuccess, "Success", "Berhasil menghapus variant item!");
              });
            })
        });
      });
  });
}
