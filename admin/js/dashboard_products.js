var parentProducts = $('#widget-products');
var parentProductsVariants = $('#widget-products-variants');

var hashProductsCategoriesRaw = {};
var hashProductsPhoto = {};

function childProducts(name, price, discount, description, productcode,
                       listOfPhotos, listOfVariantItems, listOfCategories) {
  return '' +
  '<li id="'+ productcode +'" class="has-children">\n' +
      '<a onclick="actionAccordion(this);">'+ name +'</a>\n' +
      '&nbsp;\n' +
      '<i onclick="deleteProduct(this);" class="icon-circle-minus"></i>\n' +
      '<ul>\n' +
          '<li class="product-li-name">\n' +
            '<a>Name</a>\n' +
            '<input class="form-control form-control-rounded form-control-sm widget-categories-child-input" value="'+ name +'" type="text" placeholder="Name" disabled>\n' +
          '</li>\n' +
          '<li class="product-li-price">\n' +
            '<a>Price</a>\n' +
            '<input class="form-control form-control-rounded form-control-sm widget-categories-child-input" value="'+ price +'" type="number" placeholder="Price" disabled>\n' +
          '</li>\n' +
          '<li class="product-li-discount">\n' +
            '<a>Discount</a>\n' +
            '<input class="form-control form-control-rounded form-control-sm widget-categories-child-input" value="'+ discount +'" type="number" placeholder="Discount" disabled>\n' +
          '</li>\n' +
          '<li class="product-li-description">\n' +
            '<a>Description</a>\n' +
            '<textarea class="form-control form-control-rounded form-control-sm widget-categories-child-textarea" rows="3" placeholder="Description" disabled>'+ description +'</textarea>\n' +
          '</li>\n' +
          '<li class="product-li-productcode">\n' +
            '<a>Product Code</a>\n' +
            '<input class="form-control form-control-rounded form-control-sm widget-categories-child-input" value="'+ productcode +'" type="text"  placeholder="Product code" disabled>\n' +
          '</li>\n' +
          '<li class="product-li-photos">\n' +
            '<a>Photos</a>\n' +
            '<div class="custom-file">\n' +
                '<input class="custom-file-input" onchange="productPreviewFile(this)" type="file" disabled>\n' +
                '<label class="custom-file-label" for="file-input">Tambah Photo</label>\n' +
            '</div>\n' +
            '<ul>\n' +
              listOfPhotos + '\n' +
            '</ul>\n' +
          '</li>\n' +
          '<li class="product-li-variant">\n' +
            '<a>Variant</a>\n' +
            '<ul>\n' +
              listOfVariantItems + '\n' +
            '</ul>\n' +
          '</li>\n' +
          '<li class="product-li-categories">\n' +
            '<a>Categories</a>\n' +
            '&nbsp;&nbsp;\n' +
            '<div class="safari-tags-area">\n' +
            listOfCategories+ '\n' +
            '</div>\n' +
            '<br>\n' +
          '</li>\n' +
      '</ul>\n' +
  '</li>';
}

function childEmptyProducts(listOfCategories) {
  return '' +
  '<li id="add" class="has-children">\n' +
      '<a onclick="actionAccordion(this);">Add Product</a>\n' +
      '<ul>\n' +
          '<li class="product-li-name">\n' +
            '<a>Name</a>\n' +
            // '<input class="form-control form-control-rounded form-control-sm widget-categories-child-input" type="text"  placeholder="Name">\n' +
            '<input class="form-control form-control-rounded form-control-sm widget-categories-child-input" type="text" value="test name" placeholder="Name">\n' +
          '</li>\n' +
          '<li class="product-li-price">\n' +
            '<a>Price</a>\n' +
            // '<input class="form-control form-control-rounded form-control-sm widget-categories-child-input" type="number"  placeholder="Price">\n' +
            '<input class="form-control form-control-rounded form-control-sm widget-categories-child-input" type="number" value="9999" placeholder="Price">\n' +
          '</li>\n' +
          '<li class="product-li-discount">\n' +
            '<a>Discount</a>\n' +
            // '<input class="form-control form-control-rounded form-control-sm widget-categories-child-input" type="number"  placeholder="Discount">\n' +
            '<input class="form-control form-control-rounded form-control-sm widget-categories-child-input" type="number" value="8888" placeholder="Discount">\n' +
          '</li>\n' +
          '<li class="product-li-description">\n' +
            '<a>Description</a>\n' +
            // '<textarea class="form-control form-control-rounded form-control-sm widget-categories-child-textarea" rows="3" placeholder="Description"></textarea>\n' +
            '<textarea class="form-control form-control-rounded form-control-sm widget-categories-child-textarea" rows="3" placeholder="Description">test desc</textarea>\n' +
          '</li>\n' +
          '<li class="product-li-productcode">\n' +
            '<a>Product Code</a>\n' +
            // '<input class="form-control form-control-rounded form-control-sm widget-categories-child-input" type="text"  placeholder="Product code">\n' +
            '<input class="form-control form-control-rounded form-control-sm widget-categories-child-input" type="text" value="test-0" placeholder="Product code">\n' +
          '</li>\n' +
          '<li class="product-li-photos">\n' +
            '<a>Photos</a>\n' +
            '<div class="custom-file">\n' +
                '<input class="custom-file-input" onchange="productPreviewFile(this)" type="file">\n' +
                '<label class="custom-file-label" for="file-input">Tambah Photo</label>\n' +
            '</div>\n' +
            '<ul/>\n' +
          '</li>\n' +
          '<li class="product-li-variant">\n' +
            '<a>Variant</a>\n' +
            '<ul>\n' +
              '<li>\n' +
                // '<input class="form-control form-control-rounded form-control-sm widget-categories-child-input" type="text"  placeholder="Variant Name">\n' +
                '<input class="form-control form-control-rounded form-control-sm widget-categories-child-input" type="text" value="test" placeholder="Variant Name">\n' +
                // '<textarea class="form-control form-control-rounded form-control-sm widget-categories-child-textarea" rows="3" placeholder="Variant Description"></textarea>\n' +
                '<textarea class="form-control form-control-rounded form-control-sm widget-categories-child-textarea" rows="3" placeholder="Variant Description">test</textarea>\n' +
                '<br>\n' +
                '<button onclick="addingMoreVariantItem(this)" class="btn btn-sm btn-primary" type="button">\n' +
                    'Add Variant\n' +
                    '&nbsp;\n' +
                    '<i class="icon-plus"></i>\n' +
                '</button>\n' +
              '</li>\n' +
            '</ul>\n' +
          '</li>\n' +
          '<li class="product-li-categories">\n' +
            '<a>Categories</a>\n' +
            '&nbsp;&nbsp;\n' +
            '<div class="safari-tags-area">\n' +
            listOfCategories+ '\n' +
            '</div>\n' +
            '<br>\n' +
          '</li>\n' +
          '<li class="product-li-add">\n' +
            '<button onclick="addingProducts(this);" class="btn btn-sm btn-primary" type="button">\n' +
                'Add Product\n' +
                '&nbsp;\n' +
                '<i class="icon-check"></i>\n' +
            '</button>\n' +
          '</li>\n' +
      '</ul>\n' +
  '</li>';
}

function childVariantItem(variantName, variantDescription) {
  return '' +
  '<li class="variant-added">\n' +
    '<a>'+ variantName +'</a>\n' +
    '&nbsp;\n' +
    '<i onclick="deleteVariantItem(this);" class="icon-circle-minus"></i>\n' +
    '<textarea class="form-control form-control-rounded form-control-sm widget-categories-child-textarea" rows="3" disabled>'+ variantDescription +'</textarea>\n' +
    '<br>\n' +
  '</li>\n';
}

function childVariantDisabledItem(variantName, variantDescription) {
  return '' +
  '<li class="variant-added">\n' +
    '<a>'+ variantName +'</a>\n' +
    '&nbsp;\n' +
    '<textarea class="form-control form-control-rounded form-control-sm widget-categories-child-textarea" rows="3" disabled>'+ variantDescription +'</textarea>\n' +
    '<br>\n' +
  '</li>\n';
}


function childEmptyProductsItemsTag(name) {
  return '<a onclick="actionProductsTag(this);" class="tag" value="'+ name +'">'+ name +'</a>\n';
}

function childProductsItemsTag(name) {
  return '<a onclick="actionProductsTag(this);" class="tag active" value="'+ name +'">'+ name +'</a>\n';
}

function childEmptyProductsDisabledItemsTag(name) {
  return '<a class="tag" value="'+ name +'">'+ name +'</a>\n';
}

function childProductsDisabledItemsTag(name) {
  return '<a class="tag active" value="'+ name +'">'+ name +'</a>\n';
}

function childProductsPhotoItem(url, name) {
  return ''+
  '<li>\n' +
    '<div class="media mb-3 mt-3"><img class="d-flex rounded mr-4" src="'+ url +'" width="64" alt="Media">\n' +
      '<div class="media-body">\n' +
        '<span class="d-inline-block text-sm text-muted">'+ name +'</span>\n' +
        '&nbsp;\n' +
        '<i onclick="deletePhotoItem(this);" class="icon-circle-minus"></i>\n' +
      '</div>\n' +
    '</div>\n' +
  '</li>\n';
}

function childProductsPhotoDisabledItem(url, name) {
  return ''+
  '<li>\n' +
    '<div class="media mb-3 mt-3"><img class="d-flex rounded mr-4" src="'+ url +'" width="64" alt="Media">\n' +
      '<div class="media-body">\n' +
        '<span class="d-inline-block text-sm text-muted">'+ name +'</span>\n' +
      '</div>\n' +
    '</div>\n' +
  '</li>\n';
}

// INIT STATE
function initProducts() {
  resetProducts();
  callProducts();
}

// INIT CALLING
function callProducts() {
  productsRef.get().then(function(listOfProducts) {
    if (listOfProducts.size <= 0) {
      readInitProductsCategories(function(listOfCategories) {
        parentProducts.append(childEmptyProducts(listOfCategories));
      });
    } else {
      readInitProductsCategories(function(listOfCategories) {
        readInitProducts(function() {
          parentProducts.append(childEmptyProducts(listOfCategories));
        });
      });
    }
  });
}

//
//  ADDING
//

// adding products to server
function addingProducts(view) {
  var productName = $('#add .product-li-name input');
  var productPrice = $('#add .product-li-price input');
  var productDiscount = $('#add .product-li-discount input');
  var productDescription = $('#add .product-li-description textarea');
  var productProductcode = $('#add .product-li-productcode input');
  var arrOfProductVariant = [];
  var productCategories = {};
  var arrOfPhotos = [];
  var createdAt = getTimeMillis();

  var keyProduct = productsRef.doc().id;
  var arrOfTask = [];

  eachOf('#add .product-li-photos ul li div div span', function(view) {
    var photoName = view.text();
    var fileRaw = hashProductsPhoto[photoName]
    arrOfPhotos.push(photoName);
  });

  eachOf('#add .product-li-variant .variant-added', function(view) {
    var variantName = view.children("a");
    var variantDescription = view.children("textarea");
    arrOfProductVariant.push({
      "name" : variantName.text(),
      "description" : variantDescription.val()
    });
  });

  eachOf('#add .product-li-categories .safari-tags-area a.tag.active', function(view) {
    var categoriesName = view.text();
    productCategories[categoriesName] = hashProductsCategoriesRaw[categoriesName];
  });

  if (isViewBlank(productName)) { toast(toastIconDanger, toastTypeDanger, "Alert", "Fill the Name!"); return; }
  if (isViewBlank(productPrice)) { toast(toastIconDanger, toastTypeDanger, "Alert", "Fill the Price!"); return; }
  if (!isNumberValid(productPrice.val())) { toast(toastIconDanger, toastTypeDanger, "Alert", "Fill the Price correctly!"); return; }
  if (isViewBlank(productDiscount)) { toast(toastIconDanger, toastTypeDanger, "Alert", "Fill the Discount!"); return; }
  if (!isNumberValid(productDiscount.val())) { toast(toastIconDanger, toastTypeDanger, "Alert", "Fill the Discount correctly!"); return; }
  if (isViewBlank(productDescription)) { toast(toastIconDanger, toastTypeDanger, "Alert", "Fill the Description!"); return; }
  if (isViewBlank(productProductcode)) { toast(toastIconDanger, toastTypeDanger, "Alert", "Fill the Product Code!"); return; }

  if (lengthMap(arrOfPhotos) <= 0) { toast(toastIconDanger, toastTypeDanger, "Alert", "Add photo at least 1!"); return; }
  if (lengthArray(arrOfProductVariant) <= 0) { toast(toastIconDanger, toastTypeDanger, "Alert", "Add variant at least 1!"); return; }
  if (lengthMap(productCategories) <= 0) { toast(toastIconDanger, toastTypeDanger, "Alert", "Add categories at least 1!"); return; }

  createProductsPhotoTask(keyProduct, arrOfPhotos, function(arrOfTask) {
    uploadProductsPhoto(view, arrOfTask, function(arrOfProductPhotos) {
      btnLoadingWithProgress(view, true, "uploading information");
      productsRef.doc(keyProduct).set({
        name: productName.val(),
        price: productPrice.val(),
        discount: productDiscount.val(),
        description: productDescription.val(),
        productcode: productProductcode.val(),
        variant: arrOfProductVariant,
        categories: productCategories,
        photos: arrOfProductPhotos,
        createdAt: createdAt,
        updatedAt: createdAt
      })
      .then(function() {
        var listOfVariantItems = "";
        readArray(arrOfProductVariant, function(mapOfVariant) {
          listOfVariantItems = listOfVariantItems.concat(childVariantItem(mapOfVariant["name"], mapOfVariant["description"]));
        });

        var listOfCategories = "";
        readMap(hashProductsCategoriesRaw, function(name, description) {
          if (isMapValueBlank(productCategories, name)) {
            listOfCategories = listOfCategories.concat(childEmptyProductsDisabledItemsTag(name));
          } else {
            listOfCategories = listOfCategories.concat(childProductsDisabledItemsTag(name));
          }
        });

        var listOfPhotos = "";
        var countTotal = arrOfPhotos.length;
        var countComplete = 0;
        readArray(arrOfProductPhotos, function(mapOfPhotos) {
          listOfPhotos = listOfPhotos.concat(childProductsPhotoDisabledItem(mapOfPhotos["url"], mapOfPhotos["name"]));
        });

        appendOrInsertBefore('#add', childProducts(productName.val(), productPrice.val(), productDiscount.val(),
                        productDescription.val(), productProductcode.val(), listOfPhotos,
                        listOfVariantItems, listOfCategories));

        clearInput(productName);
        clearInput(productPrice);
        clearInput(productDiscount);
        clearInput(productDescription);
        clearInput(productProductcode);

        eachOf('#add .product-li-photos ul li', function(view) {
          view.remove();
        });

        eachOf('#add .product-li-variant .variant-added', function(view) {
          view.remove();
        });

        eachOf('#add .product-li-categories .safari-tags-area a.tag.active', function(view) {
          view.removeClass("active")
        });

        btnLoadingWithProgress(view, false, null);
        toast(toastIconSuccess, toastTypeSuccess, "Success", "Berhasil menambahkan produk!");
      });
    });
  });
}

// add one variant item
function addingMoreVariantItem(view) {
  var variantParent = $(view).parent();
  var variantName = variantParent.children("input");
  var variantDescription = variantParent.children("textarea");

  if(isViewBlank(variantName)) { toast(toastIconDanger, toastTypeDanger, "Alert", "Fill the Name!"); return; }
  if(isViewBlank(variantDescription)) { toast(toastIconDanger, toastTypeDanger, "Alert", "Fill the Description!"); return; }

  $(childVariantItem(variantName.val(), variantDescription.val())).insertBefore(variantParent);

  clearInput(variantName);
  clearInput(variantDescription);
}

//
//  READ
//

function readInitProducts(onResult) {
  productsRef.orderBy("createdAt").get().then(function(listOfProducts) {
    listOfProducts.forEach(function(doc) {
      var id = doc.id;
      var name = doc.data().name;
      var price = doc.data().price;
      var discount = doc.data().discount;
      var description = doc.data().description;
      var productcode = doc.data().productcode;
      var arrOfProductPhotos = doc.data().photos;
      var variant = doc.data().variant;
      var categories = doc.data().categories;

      var listOfVariantItems = "";
      readArray(variant, function(mapOfVariant) {
        listOfVariantItems = listOfVariantItems.concat(childVariantDisabledItem(mapOfVariant["name"], mapOfVariant["description"]));
      });

      var listOfCategories = "";
      readMap(hashProductsCategoriesRaw, function(name, description) {
        if (isMapValueBlank(categories, name)) {
          listOfCategories = listOfCategories.concat(childEmptyProductsDisabledItemsTag(name));
        } else {
          listOfCategories = listOfCategories.concat(childProductsDisabledItemsTag(name));
        }
      });

      var listOfPhotos = "";

      if (arrOfProductPhotos != null) {
        var countTotal = lengthArray(arrOfProductPhotos);
        var countComplete = 0;

        readArray(arrOfProductPhotos, function(mapOfPhotos) {
          listOfPhotos = listOfPhotos.concat(childProductsPhotoDisabledItem(mapOfPhotos["url"], mapOfPhotos["name"]));
          countComplete++;
          if (countTotal == countComplete) {
            appendOrInsertBefore('#add', childProducts(name, price, discount, description, productcode,
                                         listOfPhotos, listOfVariantItems, listOfCategories));
          }
        });
      } else {
        appendOrInsertBefore('#add', childProducts(name, price, discount, description, productcode,
                                     listOfPhotos, listOfVariantItems, listOfCategories));
      }
    });
    onResult();
  });
}

// get all categories name
function readInitProductsCategories(onResult) {
  var productsCategories = "";

  hashProductsCategoriesRaw = {};
  categoriesRef.get().then(function(listOfCategories) {
    listOfCategories.forEach(function(doc) {
      hashProductsCategoriesRaw[doc.data().name] = doc.id;
      productsCategories = productsCategories.concat(childEmptyProductsItemsTag(doc.data().name));
    });
    onResult(productsCategories);
  });
}

//
//  DELETE
//

function deleteProduct(view) {
  var parent = $(view).parent();
  var parentId = parent.attr('id');

  productsRef.where("productcode", "==", parentId)
  .get().then(function(listOfProducts) {

    listOfProducts.forEach(function(doc) {
      var arrOfPhotos = doc.data().photos;
      var countComplete = 0;
      var countTotal = lengthArray(arrOfPhotos);

      var deletePhotos = function() {
        if (countComplete < countTotal) {
          productsPhotoRefStr(doc.id, arrOfPhotos[countComplete]['name'])
          .delete().then(function() {
            console.log('productPhotos/'+ doc.id +'/'+ arrOfPhotos[countComplete]['name']);

            countComplete++;
            deletePhotos();
          }).catch(function(error) {
            console.log('productPhotos/'+ doc.id +'/'+ arrOfPhotos[countComplete]['name']);

            countComplete++;
            deletePhotos();
          });;

          // console.log('productPhotos/'+ doc.id +'/'+ arrOfPhotos[countComplete]['name']);
          // countComplete++;
          // deletePhotos();
        } else {
          productsRef.doc(doc.id).delete().then(function() {
            console.log("berhasil menghapus product");
            parent.remove();
            toast(toastIconSuccess, toastTypeSuccess, "Success", "Berhasil menghapus product!");
          });

          // console.log("berhasil menghapus product");
        }
      }

      deletePhotos();
    });

  });
}

// delete one variant item
function deleteVariantItem(view) {
  var parent = $(view).parent();
  parent.remove();
}

function deletePhotoItem(view) {
  var parent = $(view).parent().parent().parent();
  var photoName = $(view).parent().children('span');

  delete hashProductsPhoto[photoName.text()];
  parent.remove();
}

//
//  ON CHANGE
//

function productPreviewFile(view) {
  var fileInput = $(view)[0];
  var fileRaw = fileInput.files[0];
  var fileName = fileRaw.name;

  if (!fileName.match(/.(jpg|jpeg|png|gif)$/i)) {
    toast(toastIconDanger, toastTypeDanger, "Alert", "Please select an Image or jpg/jpeg/png/gif!");
    return;
  }

  var parentListOfPhoto = $(view).parent().parent().children('ul');
  var reader = new FileReader();

  reader.onload = function (e) {
    parentListOfPhoto.append(childProductsPhotoItem(e.target.result, fileName));
    hashProductsPhoto[fileName] = fileRaw;
  };
  reader.readAsDataURL(fileRaw);
}

function createProductsPhotoTask(keyProduct, arrOfPhotos, onResult) {
  var countTotal = lengthArray(arrOfPhotos);
  var countComplete = 0;
  var arrOfTask = [];

  var createTask = function() {
    if (countComplete < countTotal) {
      var photoName = arrOfPhotos[countComplete];
      var fileRaw = hashProductsPhoto[arrOfPhotos[countComplete]]
      var _URL = window.URL || window.webkitURL;

      img = new Image();
      img.onload = function() {
          var metadata = {
            customMetadata: {
              'width': this.width,
              'height': this.height,
              'dimensions': this.width + "x" + this.height
            }
          };
          var task = productsPhotoRefStr(keyProduct, arrOfPhotos[countComplete]).put(fileRaw, metadata);
          arrOfTask.push([task, photoName, this.width + "x" + this.height]);
          countComplete++;
          createTask();
      };
      img.src = _URL.createObjectURL(fileRaw);
    } else {
      onResult(arrOfTask);
    }
  }

  createTask();
}

function uploadProductsPhoto(viewButton, arrOfTask, onResult) {
  var productPhotos = [];
  var countComplete = 0;
  var countTotal = arrOfTask.length;
  var indexTask = 0;
  var indexName = 1;
  var indexDimension = 2;

  var uploadPhotoTask = function() {
      if (countComplete < countTotal) {
        btnLoadingWithProgress(viewButton, true, countComplete + " of " + countTotal + " image complete");
        uploadTask(arrOfTask[countComplete][indexTask],
          function(progress) { },
          function(error) { },
          function(url) {
            var name = arrOfTask[countComplete][indexName];
            var dimension = arrOfTask[countComplete][indexDimension];
            productPhotos.push({
              "url" : url,
              "name" : name,
              "dimension" : dimension
            });
            countComplete++;
            btnLoadingWithProgress(viewButton, true, countComplete + " of " + countTotal + " image complete");
            uploadPhotoTask();
          }
        );
      } else {
        onResult(productPhotos);
      }
  };

  uploadPhotoTask();
}

//
//  RESET
//

function resetProducts() {
  parentProducts.empty();
  parentProductsVariants.empty();
}

//
//  UTILS
//

function actionProductsTag(view) {
  var target = $(view);
  target.is(".active") ? target.removeClass("active") : target.addClass("active");
}
