firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    window.location.href="dashboard";
  }
});


var onSuccess = function() {
  window.location.href="dashboard";
};

var onFailure = function(errorMessage) {
  alert(errorMessage);
};

function signIn() {
  var email = document.getElementById('email').value;
  var password = document.getElementById('password').value;

  fireSignIn(email, password, onSuccess, onFailure);
}
