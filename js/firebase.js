var fireUser = firebase.auth().currentUser;
var fireDb = firebase.firestore();
var fireStorage = firebase.storage();

// all references
var categoriesRef = fireDb.collection("categories");
var variantRef = fireDb.collection("variants");
var productsRef = fireDb.collection("products");
var helpRef = fireDb.collection("help");

var asksRef = fireDb.collection("asks");
var subscribeRef = fireDb.collection("subscribe");

var productsPhotosRefStorage = fireStorage.ref('productPhotos');

function productsPhotoRefStr(key, fileName) {
  return fireStorage.ref('productPhotos/'+ key +'/'+ fileName);
}

/**
 * Handles the sign in button press.
 */
function fireSignIn(email, password, onSuccess, onFailure) {
  if (firebase.auth().currentUser) {
    firebase.auth().signOut();
  } else {
    if (email.length < 4) {
      alert('Please enter an email address.');
      return;
    }
    if (password.length < 4) {
      alert('Please enter a password.');
      return;
    }

    firebase.auth().signInWithEmailAndPassword(email, password).then(function(){
      onSuccess();
    }).catch(function(error) {
      var errorCode = error.code;
      var errorMessage = error.message;

      if (errorCode === 'auth/wrong-password') {
        onFailure("Wrong password.");
      } else {
        onFailure(errorMessage);
      }
    });
  }
}

/**
 * Handles the sign up button press.
 */
function fireSignUp(email, password, confirmPassword, onSuccess, onFailure) {
    if (email.length < 4) {
        alert('Please enter an email address.');
        return;
    }
    if (password.length < 4) {
        alert('Please enter a password.');
        return;
    }
    if (password !== confirmPassword) {
        alert('Password is not match.');
        return;
    }

    firebase.auth().createUserWithEmailAndPassword(email, password).then(function(){
      onSuccess();
    }).catch(function(error) {
      var errorCode = error.code;
      var errorMessage = error.message;

      if (errorCode == 'auth/weak-password') {
          onFailure("The password is too weak.");
      } else {
          onFailure(errorMessage);
      }
    });
}

function fireSignOut() {
  firebase.auth().signOut();
}

/**
 * Handles the upload task.
 */
function uploadTask(task, onProgress, onError, onComplete) {
  task.on('state_changed', function(snapshot){
    var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
    onProgress(progress.toFixed(2) + '%');

    // console.log('Upload is ' + progress.toFixed(2) + '% done');

    // switch (snapshot.state) {
    //   case firebase.storage.TaskState.PAUSED: // or 'paused'
    //     // console.log('Upload is paused');
    //     break;
    //   case firebase.storage.TaskState.RUNNING: // or 'running'
    //     // console.log('Upload is running');
    //     break;
    // }
  }, function(error) {
    onError(error);
  }, function() {
    task.snapshot.ref.getDownloadURL().then(function(url) {
      onComplete(url);
    });
  });
}
