$(document).ready(function(){
  initStaticComponent(
    function() { },
    function() {
      initStaticComponentOffCanvasTitle(menuTitleGallery,
        [dataOffCanvasTitle(hrefHome, menuTitleHome)]
      );
    }
  );

  init();
});

var spGalleryPhotos = $('#sp-gallery-photos');

function init() {
  viewToLoading('#sp-gallery-photos', true);
  getAllProducts(
    function(mapOfProducts) {
      var galleryPhotos = "";
      readMap(mapOfProducts, function(key, product) {
        galleryPhotos = galleryPhotos.concat(sePhoto(product.photos));
      });
      spGalleryPhotos.append(seGalleryParent(galleryPhotos));
      scripts();
      owlCarouselDefault();

      $('#sp-gallery-photos').imagesLoaded()
      .always( function( instance ) {
        viewToLoading('#sp-gallery-photos', false);
      })
      .progress( function( instance, image ) {
        if (!image.isLoaded) {
          $(image.img).parent().parent().parent().remove();
        }
      });
    },
    function() { }
  );
}

function sePhoto(arrOfPhotos) {
  var listOfPhotos = "";
  readArray(arrOfPhotos, function(photos) {
    var name = photos.name;
    var dimension = photos.dimension;
    var url = photos.url;

    listOfPhotos = listOfPhotos.concat(
      '<div class="grid-item">\n' +
        '<div class="gallery-item">\n' +
          '<a href="'+ url +'" data-size="'+ dimension +'"><img src="'+ url +'" alt="Image"></a><span class="caption">'+ name +'</span>\n' +
        '</div>\n' +
      '</div>\n'
    );
  });
  return listOfPhotos;
}

function seGalleryParent(photos) {
  return '' +
  '<div class="isotope-grid cols-4 gallery-wrapper">\n' +
    '<div class="gutter-sizer"></div>\n' +
    '<div class="grid-sizer"></div>\n' +
    photos + '\n' +
  '</div>\n';
}
