$(document).ready(function(){
  initStaticComponent(
    function() { },
    function() {
      initStaticComponentOffCanvasTitle(menuTitleHelp,
        [dataOffCanvasTitle(hrefHome, menuTitleHome)]
      );
    }
  );

  init();
});

var spHelpSideMenu = $('#sp-help-side-menu');
var spHelpAccordion = $('#accordion');
var spHelpContainer = $('#sp-help-container');
var spHelpName = $('#help_name');
var spHelpEmail = $('#help_email');
var spHelpQuestion = $('#help_question');

var mapOfDataHelp = {};

function init() {
  viewToLoading('#accordion', true);
  getAllHelpCategories(
    function(mapOfHelpCategories) {
      var countTotal = lengthMap(mapOfHelpCategories);
      var countComplete = 0;

      var firstRowActive = true;
      var listOfSideMenu = "";
      readMap(mapOfHelpCategories, function(keyParent, dataParent) {
        getOnceQuestionAnswer(keyParent, function(mapOfAnswerQuestions) {
          var arrOfDataChild = [];
          readMap(mapOfAnswerQuestions, function(keyChild, dataChild) {
            arrOfDataChild.push({
              question: dataChild.question,
              answer: dataChild.answer,
              createdAt: dataChild.createdAt
            });
          });
          mapOfDataHelp[dataParent.name] = arrOfDataChild;

          countComplete++
          if (countComplete == countTotal) {
            generateData();
            viewToLoading('#accordion', false);
          }
        });
      });
    },
    function() { }
  );
}

function generateData() {
  var firstRowActive = true;
  var listOfSideMenu = "";
  var listOfAccordion = "";
  readMap(mapOfDataHelp, function(parentName, arrOfDataChild) {
    listOfSideMenu = listOfSideMenu.concat(seHelpSideMenu(parentName, firstRowActive));

    if (firstRowActive) {
      readArray(arrOfDataChild, function(data) {
        listOfAccordion = listOfAccordion.concat(seHelpAccordion(data));
      });
      firstRowActive = false;
    }
  });
  spHelpSideMenu.append(listOfSideMenu);
  spHelpAccordion.append(listOfAccordion);
}

function actionChangeCategories(view) {
  var currentView = $(view);
  var parent = currentView.parent();
  var categoriesValue = currentView.text();

  eachOf('#sp-help-side-menu a.active', function(view) {
    view.removeClass('active');
    spHelpAccordion.empty();
  });

  currentView.addClass('active');
  var listOfAccordion = "";
  readArray(mapOfDataHelp[categoriesValue], function(data) {
    listOfAccordion = listOfAccordion.concat(seHelpAccordion(data));
  });
  spHelpAccordion.append(listOfAccordion);
}

var onceHelpSubmitAsk = false;
function helpSubmitAsk(view) {
  if (onceHelpSubmitAsk) return;

  onceHelpSubmitAsk = true;

  if (isViewBlank(spHelpName)) { toast(toastIconDanger, toastTypeDanger, "Alert", "Fill the Name!"); return; }
  if (isViewBlank(spHelpEmail)) { toast(toastIconDanger, toastTypeDanger, "Alert", "Fill the Email!"); return; }
  if (!isEmailValid(spHelpEmail.val())) { toast(toastIconDanger, toastTypeDanger, "Alert", "Fill the Email correctly!"); return; }
  if (isViewBlank(spHelpQuestion)) { toast(toastIconDanger, toastTypeDanger, "Alert", "Fill the Question!"); return; }

  btnLoadingDefault(view, true);
  addFireData(asksRef, {
    name: spHelpName.val(),
    email: spHelpEmail.val(),
    question: spHelpQuestion.val()
  }, function() {
    toast(toastIconSuccess, toastTypeSuccess, "Success", "Berhasil mensubmit pertanyaan!");
    btnLoadingDefault(view, false);

    clearInput(spHelpName);
    clearInput(spHelpEmail);
    clearInput(spHelpQuestion);
    onceHelpSubmitAsk = false;
  });
}

function seHelpSideMenu(helpCategories, isActive) {
  var active = isActive ? 'active' : '';
  return '<a onclick="actionChangeCategories(this);" class="list-group-item '+ active +'">'+ helpCategories +'</a>\n';
}

function seHelpAccordion(data) {
  var question = data['question'];
  var answer = data['answer'];
  var id = 'id' + data['createdAt'];
  return '' +
  '<div class="card">\n' +
    '<div class="card-header" role="tab">\n' +
      '<h6><a class="collapsed" href="#'+ id +'" data-toggle="collapse">'+ question +'</a></h6>\n' +
    '</div>\n' +
    '<div class="collapse" id="'+ id +'" data-parent="#accordion" role="tabpanel">\n' +
      '<div class="card-body">\n' +
        '<p>'+ answer +'</p>\n' +
      '</div>\n' +
    '</div>\n' +
  '</div>\n';
}
