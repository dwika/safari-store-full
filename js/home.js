$(document).ready(function(){
  initStaticComponent(
    function() {
      logoSrcValue = logoSrcLight;
    },
    function() { });
  init();
});

var spProducts = $('#sp-products');
var spInstagramFeeds = $('#sp-instagram-feed');

function init() {
  viewToLoading('#sp-products', true);
  viewToLoading('#sp-instagram-feed-container', true);

  renderCategories(function() {
    renderProducts(function() {
      scripts();

      $('#sp-products').imagesLoaded()
      .always( function( instance ) {        
        viewToLoading('#sp-products', false);
        initInstagram();
      })
      .progress( function( instance, image ) {
        if (!image.isLoaded) {
          $(image.img).parent().parent().parent().remove();
        }
      });
    });
  });
}

function initInstagram() {
  initInstaFeed();
  imagesLoaded('#sp-instagram-feed-container', function () {
    viewToLoading('#sp-instagram-feed-container', false);
  });
}

function initInstaFeed() {
    var galleryFeed = new Instafeed({
        get: "user",
        userId: 10713997126,
        accessToken: "10713997126.e334c52.095458e89922416daa7ed4535ef13459",
        resolution: "standard_resolution",
        useHttp: "true",
        limit: 6,
        template: instaFeedElement,
        target: "sp-instagram-feed",
        after: function () {
            btnLoadingDefault('#sp-instagram-feed-btn', false);
            if (!this.hasNext()) {
                btnInstafeedLoad.setAttribute('disabled', 'disabled');
            }
        },
    });
    galleryFeed.run();

    var btnInstafeedLoad = document.getElementById("sp-instagram-feed-btn");
    btnInstafeedLoad.addEventListener("click", function () {
        btnLoadingDefault('#sp-instagram-feed-btn', true);
        galleryFeed.next();
    });
}

// get only categories
function renderCategories(onComplete) {
  getAllCategories(
    function(mapOfCategories) {
      var _categories = seCategories("All", true);
      readMap(mapOfCategories, function(value, key) {
        _categories = _categories.concat(seCategories(value, false));
      });
      spProducts.append(seCategoriesParent(_categories));
      onComplete();
    },
    function() { });
}

function renderProducts(onComplete) {
  getAllProducts(
    function(mapOfProducts) {
      var _products = "";
      readMap(mapOfProducts, function(key, object) {
        var _categories = "";
        readMap(object.categories, function(value, key) {
          _categories = _categories.concat(" " + value);
        });

        var _urlPhoto = object.photos[0]["url"];
        var _name = object.name;
        var _price = object.price;
        var _discount = object.discount;
        var _aHref = 'product?id=' + object.productcode;

        _products = _products.concat(seProducts(_categories, _urlPhoto, _name, _price, _discount, _aHref));
      });
      spProducts.append(seProductsParent(_products));
      onComplete();
    },
    function() { });
}

function seCategories(name, isActive) {
  var active = isActive ? 'active' : '';
  var dataFilter = name == "All" ? "*" : "." + name;
  return '' +
  '<li class="nav-item"><a class="nav-link '+ active +'" href="#" data-filter="'+ dataFilter +'">'+ name +'</a></li>';
}

function seCategoriesParent(categories) {
  return '<ul id="sp-categories" class="nav nav-pills justify-content-center pb-2">'+ categories +'</ul>';
}

function seProducts(categories, photo, name, price, discount, aHref) {
  var isDiscount = discount != "0";
  var fPrice = formatCurrency(price, currencyIDRPrefix);
  var fDiscount = formatCurrency(discount, currencyIDRPrefix);
  var vPrice = isDiscount ? '<del>'+ fPrice +'</del>&nbsp;' + fDiscount : fPrice;
  return '' +
  '<div class="grid-item '+ categories +'">\n' +
    '<div class="product-card"><a class="product-thumb" href="'+ aHref +'"><img src="'+ photo +'" alt="Products"></a>\n' +
      '<h3 class="product-title text-bold mt-4"><a href="'+ aHref +'">'+ name +'</a></h3>\n' +
      '<h4 class="product-price">'+ vPrice +'</h4>\n' +
      '<div class="product-buttons mt-4">\n' +
        '<a class="btn btn-outline-primary btn-sm" href="'+ aHref +'">View Details</a>\n' +
      '</div>\n' +
    '</div>\n' +
  '</div>';
}

function seProductsParent(products) {
  return '' +
  '<div class="isotope-grid filter-grid cols-4 mt-4">\n' +
    '<div class="gutter-sizer"></div>\n' +
    '<div class="grid-sizer"></div>\n' +
    products + '\n' +
  '</div>\n';
}

var seInstagramParent = '' +
  '<div id="sp-instagram-feed-items" class="gallery-wrapper isotope-grid cols-3 grid-no-gap">\n' +
    '<div class="gutter-sizer"></div>\n' +
    '<div class="grid-sizer"></div>\n' +
  '</div>\n';

var instaFeedElement = '' +
'<div>\n' +
  '<a href="{{link}}">\n' +
    '<div class="img-featured-container">\n' +
      '<div class="img-backdrop"></div>\n' +
      '<div class="description-container">\n' +
        '<p class="caption">{{caption}}</p>\n' +
        '<span class="likes">\n' +
          '<i class="icon icon-heart"></i> {{likes}}\n' +
        '</span>\n' +
        '<span class="likes">\n' +
          '<i class="icon icon-speech-bubble"></i> {{comments}}\n' +
        '</span>\n' +
      '</div>\n' +
      '<div class="sp-instagram-feed-img" style="background-image:url({{image}})"></div>\n' +
    '</div>\n' +
  '</a>\n' +
'</div>\n';
