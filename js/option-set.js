function existFireData(references, parameter, value, onResult, onEmpty) {
  references.where(parameter, "==", value)
  .get().then(function(listOfData) {
    if (listOfData.size <= 0) {
      onResult();
    } else {
      listOfData.forEach(function(doc) {
          onEmpty(doc);
      });
    }
  });
}

function addFireData(references, mapData, onResult) {
  references.add(mapData)
  .then(function() {
    onResult();
  });
}
