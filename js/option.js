// all categories
function getAllCategories(onResult, onEmpty) {
  categoriesRef.get().then(function(listOfCategories) {
    if (listOfCategories.size <= 0) {
      onEmpty();
    } else {
      mapOfCategories = {};
      listOfCategories.forEach(function(doc) {
        mapOfCategories[doc.data().name] = doc.id;
      });
      onResult(mapOfCategories);
    }
  });
}

// all subCategories
function getAllSubCategories(mapOfCategories, onResult) {
  var countTotal = lengthMap(mapOfCategories);
  var countComplete = 0;
  var mapOfSubCategories = {};
  readMap(mapOfCategories, function(value, key) {
    categoriesRef.doc(key).collection("subCategories")
                 .get().then(function(listOfSubCategories) {
      countComplete++;
      listOfSubCategories.forEach(function(doc) {
        mapOfSubCategories[doc.data().name] = doc.id;
      });

      if (countTotal == countComplete) {
        onResult(mapOfSubCategories);
      }
    });
  });
}

// PRODUCTS
function getAllProducts(onResult, onEmpty) {
  productsRef.get().then(function(listOfProducts) {
    if (listOfProducts.size <= 0) {
      onEmpty();
    } else {
      mapOfProducts = {};
      listOfProducts.forEach(function(doc) {
        mapOfProducts[doc.id] = doc.data();
      });
      onResult(mapOfProducts);
    }
  });
}

function getOnceProducts(productCode, onResult, onEmpty) {
  productsRef.where("productcode", "==", productCode)
  .get().then(function(listOfProducts) {
    if (listOfProducts.size <= 0) {
      onEmpty();
    } else {
      mapOfProducts = {};
      listOfProducts.forEach(function(doc) {
        mapOfProducts[doc.id] = doc.data();
      });
      onResult(mapOfProducts);
    }
  });
}

// PHOTOS
// all products photos
function getAllProductsPhoto(key, arrayOfNames, onResult, onError) {
  var countComplete = 0;
  var countTotal = lengthArray(arrayOfNames);
  var mapOfPhotos = {};

  readArray(arrayOfNames, function(name) {
    getOnceProductsPhoto(key, name,
      function(url) {
        countComplete++;
        mapOfPhotos[countComplete] = url;

        if (countTotal == countComplete) {
          onResult(mapOfPhotos);
        }
      },
      function(error) {
        onError(error);
      }
    );
  });
}

// one products photo
function getOnceProductsPhoto(key, name, onResult, onError) {
  productsPhotosRefStorage.child(key).child(name).getDownloadURL().then(function(url) {
    onResult(url);
  }).catch(function(error) {
    onError(error);
  });
}

// HELP
// queries help categories
function allHelpCategories(references, onResult, onEmpty) {
  references.get().then(function(listOfHelpCategories) {
    if (listOfHelpCategories.size <= 0) {
      onEmpty();
    } else {
      mapOfHelpCategories = {};
      listOfHelpCategories.forEach(function(doc) {
        mapOfHelpCategories[doc.id] = doc.data();
      });
      onResult(mapOfHelpCategories);
    }
  });
}

// all help categories
function getAllHelpCategories(onResult, onEmpty) {
  var references = helpRef.orderBy("updatedAt");
  allHelpCategories(references,
    function(mapOfHelpCategories) { onResult(mapOfHelpCategories); },
    function() { onEmpty(); }
  );
}

// where all help categories
function getOnceHelpCategoriesWhere(attr, symbol, value, onResult, onEmpty) {
  var references = helpRef.where(attr, symbol, value);
  allHelpCategories(references,
    function(mapOfHelpCategories) { onResult(mapOfHelpCategories); },
    function() { onEmpty(); }
  );
}

// delete once help categories
function deleteHelpCategoriesWhere(parentId, onResult) {
  getOnceHelpCategoriesWhere("createdAt", "==", Number(parentId),
    function(mapOfHelpCategories) {
      readMap(mapOfHelpCategories, function(keyParent, dataParent) {
        getOnceQuestionAnswer(keyParent, function(mapOfAnswerQuestions) {

          var arrOfKeysAnswerQuestions = [];
          var arrOfDataAnswerQuestions = [];
          readMap(mapOfAnswerQuestions, function(key, data) {
            arrOfKeysAnswerQuestions.push(key);
            arrOfDataAnswerQuestions.push(data);
          });

          var countComplete = 0;
          var countTotal = lengthArray(arrOfKeysAnswerQuestions);
          var deleteQuestionAnswerOnce = function() {
            if (countComplete < countTotal) {
              deleteQuestionAnswer(keyParent, arrOfKeysAnswerQuestions[countComplete], function() {
                countComplete++
                deleteQuestionAnswerOnce();
              });
            } else {
              helpRef.doc(keyParent).delete().then(function() {
                onResult();
              });
            }
          };

          deleteQuestionAnswerOnce();
        });
      });
    },
    function() {
      onResult();
    }
  );
}


// HELP - QUESTION AND ANSWER
// queries once help question answer
function onceQuestionAnswer(references, onResult) {
  references.get().then(function(listOfAnswerQuestions) {
    var mapOfAnswerQuestions = {};
    listOfAnswerQuestions.forEach(function(doc) {
      mapOfAnswerQuestions[doc.id] = doc.data();
    });
    onResult(mapOfAnswerQuestions);
  });
}


// where once help question answer
function getOnceQuestionAnswerWhere(attr, symbol, value, key, onResult) {
  var references = helpRef.doc(key).collection("answerQuestions")
                   .where(attr, symbol, value);
  onceQuestionAnswer(references, function(mapOfAnswerQuestions) {
    onResult(mapOfAnswerQuestions);
  });
}

// once help question answer
function getOnceQuestionAnswer(key, onResult) {
  var references = helpRef.doc(key).collection("answerQuestions").orderBy("updatedAt");
  onceQuestionAnswer(references, function(mapOfAnswerQuestions) {
    onResult(mapOfAnswerQuestions);
  });
}

// delete help question answer
function deleteQuestionAnswer(keyParent, keyChild, onResult) {
  var references = helpRef.doc(keyParent).collection("answerQuestions")
                          .doc(keyChild);

  references.delete().then(function() {
    onResult();
  });
}

// delete once help question answer
function deleteQuestionAnswerWhere(parentId, childId, onResult) {
  getOnceHelpCategoriesWhere("createdAt", "==", Number(parentId),
    function(mapOfHelpCategories) {
      readMap(mapOfHelpCategories, function(keyParent, dataParent) {
        getOnceQuestionAnswerWhere("createdAt", "==", Number(childId), keyParent,
          function(mapOfAnswerQuestions) {
            readMap(mapOfAnswerQuestions, function(keyChild, dataChild) {
              deleteQuestionAnswer(keyParent, keyChild, function() {
                onResult();
              });


            });
          }
        );
      })
    },
    function() {
      onResult();
    }
  );
}
