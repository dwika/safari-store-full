$(document).ready(function(){
  initStaticComponent(
    function() { },
    function() {
      initStaticComponentOffCanvasTitle(menuTitleProduct,
        [
          dataOffCanvasTitle(hrefHome, menuTitleHome),
          dataOffCanvasTitle(hrefOurProduct, menuTitleOurProduct)
        ]
      );
    }
  );

  init();
});

var spProducts = $('#sp-products');
var spProductsToolbar = $('#sp-products-toolbar');
var spProductsSorting = $('#sp-sorting');
var spProductsSortingShowing = $('#sp-sorting-showing');

var arrOfProducts = [];

function init() {
  viewToLoading('#sp-products', true);
  getAllProducts(
    function(mapOfProducts) {
      var listOfProducts = "";
      arrOfProducts = [];
      readMap(mapOfProducts, function(key, product) {
        arrOfProducts.push(product);
      });
      arrOfProducts.sort(dynamicSort("price"));
      resetProduct();      

      $('#sp-products').imagesLoaded()
      .always( function( instance ) {
        viewToLoading('#sp-products', false);
      })
      .progress( function( instance, image ) {
        if (!image.isLoaded) {
          $(image.img).parent().parent().parent().remove();
        }
      });
    },
    function() { }
  );
}

function resetProduct() {
  var listOfProducts = "";
  readArray(arrOfProducts, function(product) {
    var _categories = "";
    readMap(product.categories, function(value, key) {
      _categories = _categories.concat(" " + value);
    });

    var _urlPhoto = product.photos[0]["url"];
    var _name = product.name;
    var _price = product.price;
    var _discount = product.discount;
    var _aHref = 'product?id=' + product.productcode;

    listOfProducts = listOfProducts.concat(seProducts(_categories, _urlPhoto, _name, _price, _discount, _aHref));
  });
  spProducts.append(seProductsParent(listOfProducts));

  imagesLoaded('#sp-products', function() {
    spProductsSortingShowing.append(seProductsShowing("Showing", "1 - " + lengthArray(arrOfProducts) + " items"));
    actionSorting();
    scripts();
  });
}

var optionLowHigh = "low-high";
var optionHighLow = "high-low";
var optionAZ = "a-z";
var optionZA = "z-a";

function actionSorting() {
  spProductsSorting.on('change', function() {
    spProductsSortingShowing.empty();
    spProducts.empty();
    spProductsSorting.unbind('change');

    switch(this.value) {
      case optionLowHigh:
        arrOfProducts.sort(dynamicSort("price"));
        resetProduct();
        break;
      case optionHighLow:
        arrOfProducts.sort(dynamicSort("-price"));
        resetProduct();
        break;
      case optionAZ:
        arrOfProducts.sort(dynamicSort("name"));
        resetProduct();
        break;
      case optionZA:
        arrOfProducts.sort(dynamicSort("-name"));
        resetProduct();
        break;
      default:
        arrOfProducts.sort(dynamicSort("price"));
        resetProduct();
    }
  });
}

function seProducts(categories, photo, name, price, discount, aHref) {
  var isDiscount = discount != "0";
  var fPrice = formatCurrency(price, currencyIDRPrefix);
  var fDiscount = formatCurrency(discount, currencyIDRPrefix);
  var vPrice = isDiscount ? '<del>'+ fPrice +'</del>&nbsp;' + fDiscount : fPrice;
  return '' +
  '<div class="grid-item '+ categories +'">\n' +
    '<div class="product-card"><a class="product-thumb" href="'+ aHref +'"><img src="'+ photo +'" alt="Products"></a>\n' +
      '<h3 class="product-title text-bold mt-4"><a href="'+ aHref +'">'+ name +'</a></h3>\n' +
      '<h4 class="product-price">'+ vPrice +'</h4>\n' +
      '<div class="product-buttons mt-4">\n' +
        '<a class="btn btn-outline-primary btn-sm" href="'+ aHref +'">View Details</a>\n' +
      '</div>\n' +
    '</div>\n' +
  '</div>';
}

function seProductsParent(products) {
  return '' +
  '<div class="isotope-grid filter-grid cols-4 mb-2">\n' +
    '<div class="gutter-sizer"></div>\n' +
    '<div class="grid-sizer"></div>\n' +
    products + '\n' +
  '</div>\n';
}

function seProductsPagination(lengthMap) {
  return '' +
  '<nav class="pagination">\n' +
    '<div class="column">\n' +
      '<ul class="pages">\n' +
        '<li class="active"><a href="#">1</a></li>\n' +
      '</ul>\n' +
    '</div>\n' +
    '<div class="column text-right hidden-xs-down">\n' +
      '<a class="btn btn-outline-secondary btn-sm" href="#">\n' +
        'Next&nbsp;\n' +
        '<i class="icon-arrow-right"></i>\n' +
      '</a>\n' +
    '</div>\n' +
  '</nav>\n';
}

function seProductsShowing(labelShowing, labelItems) {
  return '' +
  '<span class="text-muted">'+ labelShowing +':&nbsp;</span>\n' +
  '<span>'+ labelItems +'</span>\n';
}
