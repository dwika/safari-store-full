$(document).ready(function(){
  initStaticComponent(
    function() { },
    function() {
      initStaticComponentOffCanvasTitle(menuTitleProduct,
        [
          dataOffCanvasTitle(hrefHome, menuTitleHome),
          dataOffCanvasTitle(hrefOurProduct, menuTitleOurProduct)
        ]
      );
    }
  );
  init();
});

var urlData = "id";
var spProducts = $('#sp-products');
var spProductsPhotos = $('#sp-products-photos');
var spProductsData = $('#sp-products-data');
var spProductsAlsolike = $('#sp-products-alsolike');

function init() {
  viewToLoading('#sp-products', true);
  getUrl(function(mapOfDataUrl) {
    var productCode = mapOfDataUrl[urlData];
    getOnceProducts(productCode, function(mapOfProducts) {
      readMap(mapOfProducts, function(key, object) {
        spProductsPhotos.append(seProductsPhotos(object.photos));
        spProductsData.append(seProductsData(object));
        // scripts();

        imagesLoaded('#sp-products', function() {
          viewToLoading('#sp-products', false);
            scripts();          
        });
      });
    },
    function() {
      window.location.href="blank";
    });
  });

  viewToLoading('#sp-products-alsolike', true);
  getAllProducts(
    function(mapOfProducts) {
      var productAlsolike = "";
      readMap(mapOfProducts, function(key, product) {
        productAlsolike = productAlsolike.concat(seProductAlsoLike(product));
      });
      spProductsAlsolike.append(productAlsolike);

      imagesLoaded('#sp-products-alsolike', function() {
        viewToLoading('#sp-products-alsolike', false);
        owlCarouselDefault();
      });
    },
    function() { }
  );
}

function seProductAlsoLike(product) {
  var href = hrefProduct + "?id=" + product.productcode;
  var src = product.photos[0].url;
  var name = product.name;

  var isDiscount = product.discount != "0";
  var fPrice = formatCurrency(product.price, currencyIDRPrefix);
  var fDiscount = formatCurrency(product.discount, currencyIDRPrefix);
  var vPrice = isDiscount ? '<del class="text-muted text-normal">'+ fPrice +'</del> &nbsp;' + fDiscount : fPrice;

  return '' +
  '<div class="grid-item">\n' +
    '<div class="product-card">\n' +
      '<a class="product-thumb" href="'+ href +'"><img src="'+ src +'" alt="Product"></a>\n' +
      '<h3 class="product-title text-bold"><a href="'+ href +'">'+ name +'</a></h3>\n' +
      '<h4 class="product-price">\n' +
        vPrice +
      '</h4>\n' +
      '<div class="product-buttons">\n' +
        '<a class="btn btn-outline-primary btn-sm" href="'+ href +'">View Details</a>\n' +
      '</div>\n' +
    '</div>\n' +
  '</div>\n';
}

function seProductsPhotos(arrOfPhotos) {
  var carousel = "";
  var thumbnail = "";
  readArray(arrOfPhotos, function(mapOfPhotos) {
    var url = mapOfPhotos["url"];
    var name = mapOfPhotos["name"];
    var dimension = mapOfPhotos["dimension"];

    carousel = carousel.concat('<div class="gallery-item" data-hash="'+ name +'"><a href="'+ url +'" data-size="'+ dimension +'"><img src="'+ url +'" alt="Product"></a></div>\n');

    if (isValueBlank(thumbnail)) {
      thumbnail = thumbnail.concat('<li class="active"><a href="#'+ name +'"><img src="'+ url +'" alt="Product"></a></li>\n');
    } else {
      thumbnail = thumbnail.concat('<li><a href="#'+ name +'"><img src="'+ url +'" alt="Product"></a></li>\n');
    }
  });

  return '' +
  '<div class="product-gallery">\n' +
    '<div class="product-carousel owl-carousel gallery-wrapper mt-4">\n' +
      carousel +
    '</div>\n' +
    '<ul class="product-thumbnails">\n' +
      thumbnail +
    '</ul>\n' +
  '</div>';
}

function seProductsData(products) {
  var isDiscount = products.discount != "0";
  var fPrice = formatCurrency(products.price, currencyIDRPrefix);
  var fDiscount = formatCurrency(products.discount, currencyIDRPrefix);
  var vPrice = isDiscount ? '<del class="text-muted text-normal">'+ fPrice +'</del> &nbsp;' + fDiscount : fPrice;

  var vVariant = seProductsVariant(products);
  var whatsapp = sosmedWA(sosmedNumber, "Saya tertarik dengan produk " + products.name + " (" +products.productcode + ")");
  return '' +
  '<div class="padding-top-2x mt-2 hidden-md-up"></div>\n' +
  '<h2 class="padding-top-1x text-bold mb-3">'+ products.name +'</h2>\n' +
  '<span class="h3 d-block text-bold font-cusize">'+ vPrice +'</span>\n' +
  '<p class="mt-5">'+ products.description +'</p>\n' +
  '<div class="row mb-30 mt-4">\n' +
    vVariant +
  '</div>\n' +
  '<div class="row margin-top-1x">\n' +
    '<div class="col-sm-12">\n' +
      '<div class="sp-buttons mt-4 mb-4">\n' +
        '<a class="btn btn-primary width-100" href="'+ whatsapp +'" target="_blank"></i>Beli Sekarang</a>\n' +
      '</div>\n' +
    '</div>\n' +
  '</div>\n' +
  '<hr class="mb-3">\n' +
  '<div class="d-flex flex-wrap justify-content-between">\n' +
    '<div class="entry-share mt-2 mb-2"><span class="text-muted">Share:</span>\n' +
      '<div class="share-links">\n' +
        '<a class="social-button shape-circle sb-facebook" href="https://www.facebook.com/safari.tandukrusa" data-toggle="tooltip" data-placement="top" title="Facebook">\n' +
          '<i class="socicon-facebook"></i>\n' +
        '</a>\n' +
        '<a class="social-button shape-circle sb-instagram" href="https://instagram.com/safaristore.id?utm_source=ig_profile_share&igshid=uf22c1itffo3" data-toggle="tooltip" data-placement="top" title="Instagram">\n' +
          '<i class="socicon-instagram"></i>\n' +
        '</a>\n' +
      '</div>\n' +
    '</div>\n' +
  '</div>';
}

function seProductsVariant(products) {
  var variant = "";
  var groupVariant = "";
  var countComplete = 0;
  var countTotal = lengthMap(products.variant) + 1;
  var countHalfComplete = Math.ceil(countTotal/2);
  readArray(products.variant, function(mapOfVariant) {
    var name = mapOfVariant["name"];
    var description = mapOfVariant["description"];

    if (isValueBlank(variant) && countComplete < countHalfComplete) {
      variant = variant.concat(
        '<dt>Product Code:</dt>\n' +
        '<dd>'+ products.productcode +'</dd>\n'
      );
      countComplete++;
    }

    variant = variant.concat(
      '<dt>'+ name +':</dt>\n' +
      '<dd>'+ description.replace(/(?:\r\n|\r|\n)/g, '<br>') +'</dd>\n'
    );

    countComplete++;
    if (countComplete == countHalfComplete) {
      groupVariant = groupVariant.concat('' +
      '<div class="col-sm-6">\n' +
        '<dl>\n' +
        variant +
        '</dl>\n' +
      '</div>\n');
      variant = "";
    } else if (countComplete == countTotal) {
      groupVariant = groupVariant.concat('' +
      '<div class="col-sm-6">\n' +
        '<dl>\n' +
        variant +
        '</dl>\n' +
      '</div>\n');
      variant = "";
    }
  });

  return groupVariant;
}
