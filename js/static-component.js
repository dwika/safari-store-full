var staticSPHead = $('head');

var staticSPOffCanvas = $('.offcanvas-container');
var staticSPTopbar = $('.topbar');
var staticSPHeader = $('.navbar');
var staticSPOffCanvasWrapper = $('.offcanvas-wrapper');
var staticSPFooter = $('.site-footer');

function initStaticComponent(onStarter, onComplete) {
  onStarter();
  staticSPHead.prepend(staticHead);

  staticSPOffCanvas.append(staticOffCanvas);
  staticSPTopbar.append(staticTopbar);
  staticSPHeader.append(staticHeader());
  staticSPFooter.append(staticFooter);

  onComplete();
}

var onceActionStaticToSubscribe = false;
function actionStaticToSubscribe(view) {
  if (onceActionStaticToSubscribe) return;

  onceActionStaticToSubscribe = true;

  var staticSPFooterSubscribe = $('#sp-footer-subscribe');

  if (isViewBlank(staticSPFooterSubscribe)) { toast(toastIconDanger, toastTypeDanger, "Alert", "Fill the email!"); return; }
  if (!isEmailValid(staticSPFooterSubscribe.val())) { toast(toastIconDanger, toastTypeDanger, "Alert", "Fill the Email correctly!"); return; }

  btnLoading(view, true);
  addFireData(subscribeRef, {
    email: staticSPFooterSubscribe.val(),
  }, function() {
    toast(toastIconSuccess, toastTypeSuccess, "Success", "Berhasil Subscribe!");
    btnLoading(view, false);

    clearInput(staticSPFooterSubscribe);
    onceActionStaticToSubscribe = false;
  });
}

var hrefTandukRusa = "http://tandukrusa.com/";
var hrefProduct = "product";
var hrefHome = "/";
var hrefOurProduct = "our-product";
var hrefGallery = "gallery";
var hrefHelp = "help"
var hrefAbout = "about"

var menuTitleProduct = "Product Details";
var menuTitleHome = "Safari";
var menuTitleOurProduct = "Our Product";
var menuTitleGallery = "Gallery";
var menuTitleHelp = "Help";
var menuTitleAbout = "About Us";

var logoSrcLight = "img/logo/logo-light.png";
var logoSrcBlue = "img/logo/logo.png";
var logoSrcValue = logoSrcBlue;

// SOSMED
var sosmedNumber = "6282141242613";
var sosmedInstagram = "https://www.instagram.com/safaristore.id/";
var sosmedFacebook = "https://www.facebook.com/safari.tandukrusa";
var sosmedEmailCS = "cs@tandukrusa.com";

function sosmedWA(number, text) {
  return 'https://wa.me/'+ number +'?text=' + text;
}

function dataOffCanvasTitle(href, menuTitle) {
  return {'href': href, 'menuTitle': menuTitle};
}

function initStaticComponentOffCanvasTitle(title, arrOfLink) {
  var listOfLink = "";
  readArray(arrOfLink, function(mapOfMenus) {
    var href = mapOfMenus['href'];
    var menuTitle = mapOfMenus['menuTitle'];
    listOfLink = listOfLink.concat('' +
      '<li>\n' +
        '<a href="'+ href +'">'+ menuTitle +'</a>\n' +
      '</li>\n' +
      '<li class="separator">&nbsp;</li>\n'
    );
  });
  listOfLink = listOfLink.concat('<li>'+ title +'</li>\n');
  staticSPOffCanvasWrapper.prepend(
    '<div class="page-title">\n' +
      '<div class="container">\n' +
        '<div class="column">\n' +
          '<h1>'+ title +'</h1>\n' +
        '</div>\n' +
        '<div class="column">\n' +
          '<ul class="breadcrumbs">\n' +
            listOfLink + '\n' +
          '</ul>\n' +
        '</div>\n' +
      '</div>\n' +
    '</div>\n'
  );
}

var staticHead = '' +
'<meta charset="utf-8">\n' +
'<title>Safari | Kali Ini Beda!\n' +
'</title>\n' +
'<!-- SEO Meta Tags-->\n' +
'<meta name="description" content="Safari">\n' +
'<meta name="keywords" content="shop, e-commerce, modern, flat style, responsive, online store, business, mobile, blog, creative, apparel">\n' +
'<meta name="author" content="Safari">\n' +
'<!-- Mobile Specific Meta Tag-->\n' +
'<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">\n' +
'<!-- Favicon and Apple Icons-->\n' +
'<link rel="icon" type="image/x-icon" href="favicon.ico">\n' +
'<link rel="icon" type="image/png" href="favicon.png">\n' +
'<link rel="apple-touch-icon" href="touch-icon-iphone.png">\n' +
'<link rel="apple-touch-icon" sizes="152x152" href="touch-icon-ipad.png">\n' +
'<link rel="apple-touch-icon" sizes="180x180" href="touch-icon-iphone-retina.png">\n' +
'<link rel="apple-touch-icon" sizes="167x167" href="touch-icon-ipad-retina.png">\n';

var staticOffCanvas = '' +
'<nav class="offcanvas-menu">\n' +
  '<ul class="menu">\n' +
    '<li><a href="'+ hrefOurProduct +'"><span>'+ menuTitleOurProduct +'s</span></a></li>\n' +
    '<li><a href="'+ hrefGallery +'"><span>'+ menuTitleGallery +'</span></a></li>\n' +
    '<li><a href="'+ hrefHelp +'"><span>'+ menuTitleHelp +'</span></a></li>\n' +
    '<li><a href="'+ hrefAbout +'"><span>'+ menuTitleAbout +'</span></a></li>\n' +
  '</ul>\n' +
'</nav>\n';

var staticTopbar = '' +
'<div class="topbar-column">\n' +
  '<a class="hidden-md-down" href="mailto:'+ sosmedEmailCS +'">\n' +
    '<i class="icon-mail"></i>\n' +
    '&nbsp; '+ sosmedEmailCS +'\n' +
  '</a>\n' +
  '<a class="hidden-md-down" href="tel:00331697720">\n' +
    '<i class="icon-bell"></i>\n' +
    '&nbsp; 0821 4124 2613\n' +
  '</a>\n' +
  '<a class="social-button sb-facebook shape-none sb-light-skin" href="'+ sosmedFacebook +'" target="_blank">\n' +
    '<i class="socicon-facebook"></i>\n' +
  '</a>\n' +
  '<a class="social-button sb-instagram shape-none sb-light-skin" href="'+ sosmedInstagram +'" target="_blank">\n' +
    '<i class="socicon-instagram"></i>\n' +
  '</a>\n' +
'</div>\n' +
'<div class="topbar-column">\n' +
  '<a class="hidden-md-down" href="'+ hrefTandukRusa +'" target="_blank">\n' +
    '<i class="icon-open"></i>\n' +
    '&nbsp; to Tanduk Rusa\n' +
  '</a>\n' +
'</div>\n';

function staticHeader() {
  return '' +
  '<!-- Search-->\n' +
  '<!-- <form class="site-search" method="get">\n' +
    '<input type="text" name="site_search" placeholder="Type to search...">\n' +
    '<div class="search-tools"><span class="clear-search">Clear</span><span class="close-search"><i class="icon-cross"></i></span></div>\n' +
  '</form> -->\n' +
  '<div class="site-branding">\n' +
    '<div class="inner">\n' +
      '<a class="offcanvas-toggle menu-toggle" href="#mobile-menu" data-toggle="offcanvas"></a>\n' +
      '<a class="site-logo light-logo" href="'+ hrefHome +'"><img src="'+ logoSrcValue +'" alt="Safari"></a><a class="site-logo logo-stuck" href="'+ hrefHome +'"><img src="'+ logoSrcBlue +'" alt="Safari"></a>\n' +
    '</div>\n' +
  '</div>\n' +
  '<!-- Main Navigation-->\n' +
  '<nav class="site-menu">\n' +
    '<ul>\n' +
      '<li><a href="'+ hrefOurProduct +'"><span>'+ menuTitleOurProduct +'s</span></a></li>\n' +
      '<li><a href="'+ hrefGallery +'"><span>'+ menuTitleGallery +'</span></a></li>\n' +
      '<li><a href="'+ hrefHelp +'"><span>'+ menuTitleHelp +'</span></a></li>\n' +
      '<li><a href="'+ hrefAbout +'"><span>'+ menuTitleAbout +'</span></a></li>\n' +
    '</ul>\n' +
  '</nav>\n' +
  '<!-- Toolbar-->\n' +
  '<!-- <div class="toolbar">\n' +
    '<div class="inner">\n' +
      '<div class="tools">\n' +
        '<div class="search"><i class="icon-search"></i></div>\n' +
      '</div>\n' +
    '</div>\n' +
  '</div> -->\n';
}

var staticFooter = '' +
'<div class="container">\n' +
  '<div class="row">\n' +
    '<div class="col-lg-3 col-md-6">\n' +
      '<!-- Contact Info-->\n' +
      '<section class="widget widget-light-skin">\n' +
        '<h3 class="widget-title">Get In Touch With Us</h3>\n' +
        '<p class="text-white"><a class="navi-link-light" href="tel:'+ sosmedNumber +'">Phone: 0821 4124 2613</a></p>\n' +
        '<p> <a class="navi-link-light" href="mailto:'+ sosmedEmailCS +'">Email: '+ sosmedEmailCS +'</a></p>\n' +
        '<ul class="list-unstyled text-sm text-white">\n' +
          '<li><span class="opacity-50">Monday-Sunday:</span>24 hours</li>\n' +
        '</ul>\n' +
      '</section>\n' +
    '</div>\n' +
    '<div class="col-lg-3 col-md-6">\n' +
      '<!-- Mobile App Buttons-->\n' +
      '<section class="widget widget-light-skin">\n' +
        '<h3 class="widget-title">Our Mobile App</h3>\n' +
        '<a class="social-button shape-circle sb-facebook sb-light-skin" href="'+ sosmedFacebook +'"><i class="socicon-facebook"></i></a><span class="mb-subtitle">Facebook</span> </br>\n' +
        '<a class="social-button shape-circle sb-instagram sb-light-skin" href="'+ sosmedInstagram +'"><i class="socicon-instagram"></i></a><span class="mb-subtitle">Instagram</span>\n' +
        '</a>\n' +
      '</section>\n' +
    '</div>\n' +
    '<div class="col-lg-6 col-md-6">\n' +
      '<section class="widget widget-light-skin">\n' +
        '<div class="subscribe-form">\n' +
          '<div class="clearfix">\n' +
            '<div class="input-group input-light">\n' +
              '<input id="sp-footer-subscribe" class="form-control" type="email" name="EMAIL" placeholder="Your e-mail"><span class="input-group-addon"><i class="icon-mail"></i></span>\n' +
            '</div>\n' +
            '<button onclick="actionStaticToSubscribe(this);" class="btn btn-primary" type="submit"><i class="icon-check"></i></button>\n' +
          '</div><span class="form-text text-sm text-white opacity-50">Subscribe to our Newsletter to receive early discount offers, latest news, sales and promo information.</span>\n' +
        '</div>\n' +
      '</section>\n' +
    '</div>\n' +
  '</div>\n' +
  '<hr class="hr-light mt-2 margin-bottom-2x">\n' +
  '<!-- Copyright-->\n' +
  '<p class="footer-copyright">© All rights reserved. Made with &nbsp;<i class="icon-heart text-danger"></i><a href="'+ hrefTandukRusa +'" target="_blank"> &nbsp;by Tanduk Rusa</p>\n' +
'</div>\n';
