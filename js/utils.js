// CLEAR INPUT VALUE
function clearInput(view) {
  $(view).val("");
}

// TOAST
var toastTypeSuccess = "success";
var toastTypeInfo = "info";
var toastTypeDanger = "danger";
var toastTypeWarning = "warning";

var toastIconSuccess = "icon-circle-check";
var toastIconInfo = "icon-bell";
var toastIconDanger = "icon-ban";
var toastIconWarning = "icon-flag";

function toast(icon, type, title, message) {
  iziToast.show({
    title: title,
    message: message,
    position: "topRight",
    class: "iziToast-" + type,
    icon: 'icon-circle-check',
    timeout: 3200,
    transitionIn: "fadeInLeft",
    transitionOut: "fadeOut",
    transitionInMobile: "fadeIn",
    transitionOutMobile: "fadeOut"
  });
}

// CAROUSEL
function owlCarouselDefault() {
  $('.spc-owl-carousel').owlCarousel({
    nav: false,
    dots: true,
    margin: 30,
    responsive: {
      0:{
        items:1
      },
      576:{
        items:2
      },
      768:{
        items:3
      },
      991:{
        items:4
      },
      1200:{
        items:4
      }
    }
  });
}


// EXPAND / HIDE ACCORDION
function actionAccordion(view) {
  var accordionView = $(view).parent();
  accordionView.is(".expanded") ? accordionView.removeClass("expanded") : accordionView.addClass("expanded");
}

// CHECK IF NULL
function isViewBlank(view) {
  return $.trim(view.val()) === "" ? true : false;
}

function isValueBlank(value) {
  return $.trim(value) === "" ? true : false;
}

function isMapValueBlank(map, key) {
  return isValueBlank(map[key]);
}

// COUNT MAP SIZE
function lengthMap(map) {
  return Object.keys(map).length;
}

function lengthArray(array) {
  return array.length;
}

// COUNT EACH
function eachOf(element, onResult) {
  $(element).each(function() {
      onResult($(this));
  });
}

// READ MAP
function readMap(map, onResult) {
  for (var key in map) {
    onResult(key, map[key]);
  }
}

function readMapWithCompletion(map, onResult) {
  var countComplete = 0;
  var countTotal = lengthArray(array);
  readMap(array, function(key, value) {
    onResult(key, value, false);

    countComplete++;
    if (countComplete == countTotal) {
      onResult(key, value, true);
    }
  });
}

function readMapKeyIndex(map, index) {
  return Object.keys(map)[index];
}

// READ ARRAY
function readArray(array, onResult) {
  array.forEach(function(item) {
    onResult(item);
  });
}

function readArrayWithCompletion(array, onResult) {
  var countComplete = 0;
  var countTotal = lengthArray(array);
  readArray(array, function(item) {
    onResult(item, false);

    countComplete++;
    if (countComplete == countTotal) {
      onResult(item, true);
    }
  });
}

// SORT DYNAMIC ARRAY OF MAP
function dynamicSort(property) {
    var sortOrder = 1;
    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a,b) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    }
}

// INNER SPINNER
var spinnerValue = '<div class="spinner-border spinner-border-sm text-white" role="status"></div>';
var checkValue = '<i class="icon-check"></i>';

function progressValue(progress) {
  return '<span>'+ progress +'&nbsp;</span>';
}

function btnLoading(view, isLoading) {
  if (isLoading) {
    $(view).children('i').replaceWith(spinnerValue);
  } else {
    $(view).children('div').replaceWith(checkValue);
  }
}

function btnLoadingDefault(view, isLoading) {
  if (isLoading) {
    $(view).append(spinnerValue);
  } else {
    $(view).children('div').remove();
  }
}

function btnLoadingWithProgress(view, isLoading, percentage) {
  var progress = $(view).children('span');
  var icon = $(view).children('i');
  var loading = $(view).children('div');

  if (isLoading) {
    if (progress.exists()) {
      progress.replaceWith(progressValue(percentage));
    } else {
      $(progressValue(percentage)).insertBefore(icon);
    }
    icon.replaceWith(spinnerValue);
  } else {
    progress.remove();
    loading.replaceWith(checkValue);
  }
}

// OUTER SPINNER
var loadingViewClass = '.sf-loading';
var loadingView = '' +
  '<div class="sf-loading text-center">\n' +
    '<div class="spinner-grow text-gray-dark m-2" style="width: 3rem; height: 3rem;" role="status"></div>\n' +
  '</div>';

// ADD, INSERT, REMOVE, AND REPLACE
function appendOrInsertBefore(view, target) {
  if ($(view).length) {
    $(target).insertBefore($(view));
  } else {
    parentProducts.append(target);
  }
}

function applyOrRemove(parent, target, view, isVisible) {
  var parentView = $(parent);
  applyOrRemoveView(parentView, target, view, isVisible);
}

function applyOrRemoveView(parentView, view, target, targetView, isVisible) {
  var children = parentView.children(target);

  if (children.length) {
    if (!isVisible) {
      children.remove();
    }
  } else {
    $(targetView).insertBefore(view);
  }
}

function viewToLoading(view, isLoading) {
  var parentView = $(view).parent();
  if (isLoading) {
    showView(view, false);
    applyOrRemoveView(parentView, view, loadingViewClass, loadingView, true);
  } else {
    showView(view, true);
    applyOrRemoveView(parentView, view, loadingViewClass, loadingView, false);
  }
}

// IMAGES LOADED
function imagesLoaded(container, onAllDone) {
  $(container).imagesLoaded()
  .done( function( instance ) {
    onAllDone();
  });
}

function urlExists(url){
  $.get(url)
  .done(function() {
    console.log("exist!");
  }).fail(function() {
    console.log("404!");
  })
}

// SHOW / HIDE
function showView(view, isShow) {
  var spView = $(view);
  showSPView(spView, isShow);
}

function showSPView(view, isShow) {
  if (isShow) {
    view.css("opacity", "unset");
    view.css("height", "unset");
    view.css("pointer-events", "unset");
  } else {
    view.css("opacity", "0");
    view.css("height", "0");
    view.css("pointer-events", "none");
  }
}

// REGEX URL TO GET VALUE
function getUrl(onResult) {
  var queryDict = {}
  location.search.substr(1).split("&").forEach(
    function(item) {
      var data = item.split("=");
      var key = data[0];
      var value = data[1];
      queryDict[key] = value;
    }
  )
  onResult(queryDict);
}

// CURRENCY
var currencyIDRPrefix = 'Rp'
function formatCurrency(number, currency) {
  return currency + " " + number.toString().replace(/./g, function(c, i, a) {
    return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
  });
}

// VALIDATE EMAIL
function isEmailValid(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

// VALIDATE NUMBER
function isNumberValid(number) {
  return !isNaN(parseFloat(number)) && isFinite(number);
}

// GET TIME MILLIS
function getTimeMillis() {
  var date = new Date();
  var timeMillis = date.getTime();
  return timeMillis;
}

// EXTENSION

$.fn.exists = function () {
    return this.length !== 0;
}

// TEST PRINT MAP
function debugLogMap(map) {
  var value = "";
  for (var key in map) {
    value = value.concat(key + " ==> " + map[key] + "\n")
  }
  return value;
}

function debugLogArray(array) {
  var value = "";
  var i = 0;
  for (i = 0; i < array.length; ++i) {
    value = value.concat(array[i].price + "\n")
  }
  return value;
}

// sp = safari-pointer
// se = safari-element

// var progress = 0;
// var intervalID = setInterval(function() {
//   progress += 5;
//   btnLoadingWithProgress(view, true, progress + "%");
// }, 100);
// setTimeout(function() {
//   clearInterval(intervalID);
//   btnLoadingWithProgress(view, false, null);
// }, 2000);

// eachOf('#add .product-li-photos ul li div div span', function(view) {
//   var photoName = view.text();
//
//   console.log(photoName + "  " + hashProductsPhoto[photoName].name);
// });
// console.log(debugLogMap(hashProductsPhoto));
//
// return;
